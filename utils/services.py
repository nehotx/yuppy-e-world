from utils.pictures import *
from utils.db import db
from models.models import *

def update_enterprise_picture(username,picture_name_new):
    enterprise = Enterprises.query.filter_by(username = username).first()
    if enterprise.logo is not None:
        remove_enterprise_picture(enterprise.logo)
    
    enterprise.logo = picture_name_new
    
    db.session.commit()

def update_user_picture(dni,picture_name_new):
    user = Users.query.filter_by(dni = dni).first()
    if user.logo is not None:
        remove_user_picture(user.logo)
    
    user.logo = picture_name_new
    
    db.session.commit()


def update_product_picture(product_id,picture_name_new):
    image = Products_images.query.filter_by(product_id = product_id).first()
    if image is None:
        filename = picture_name_new
        product_id = product_id
        new_product_img = Products_images(filename, product_id)
        db.session.add(new_product_img)
        db.session.commit()

    elif image.filename is not None:
        remove_product_picture(image.filename)
        image.filename = picture_name_new
    
        db.session.commit()
