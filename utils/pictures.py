from os import  path, remove
import datetime

def path_url_exists(path_url):
    if path.exists(path_url):
        return True
    return False

def remove_product_picture(product_id):
    path_url = "uploads/products_pictures/" + product_id
    if path_url_exists(path_url):
        remove(path_url)

def remove_enterprise_picture(profile_name):
    path_url = "uploads/profile_pictures/enterprises/" + profile_name
    if path_url_exists(path_url):
        remove(path_url)

def remove_user_picture(profile_name):
    path_url = "uploads/profile_pictures/users/" + profile_name
    if path_url_exists(path_url):
        remove(path_url)

def random_name(name_base):
    suffix = datetime.datetime.now().strftime("%y%m%d_%H%M%S")
    filename = '_'.join([name_base, suffix])
    return filename