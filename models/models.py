from utils.db import db

import datetime    

#Models and migrations

class Countries(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), unique=True)
    states = db.relationship('States', backref='states_in_this_country',cascade='all, delete-orphan')
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow)
    updated_at = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)

    def __init__(self,name):
        self.name = name.title()

class States(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    country_id = db.Column(db.Integer, db.ForeignKey(Countries.id, ondelete='CASCADE'), nullable=False)
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow)
    updated_at = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)

    def __init__(self,name,country_id):
        self.name = name.title()
        self.country_id = country_id

class Users(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), unique=False, nullable=False)
    lastname = db.Column(db.String(50), unique=False, nullable=False)
    dni = db.Column(db.String(50), unique=True, nullable=False)
    email = db.Column(db.String(150), unique=True, nullable=False)
    phone = db.Column(db.String(150), unique=True, nullable=True)
    password = db.Column(db.String(80), nullable=False)
    logo = db.Column(db.String(200), nullable=True)
    description = db.Column(db.String(500),nullable=True)
    level = db.Column(db.Integer, default=1)
    calification = db.Column(db.Float, default=0)
    comments = db.relationship('Comments', backref='comment_owner', cascade='all, delete-orphan')
    country_id = db.Column(db.Integer, db.ForeignKey(Countries.id, ondelete='CASCADE'), nullable=True)
    state_id = db.Column(db.Integer, db.ForeignKey(States.id, ondelete='CASCADE'), nullable=True)
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow)
    updated_at = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    
    def __init__(self,name,lastname,dni,email,password,country_id,state_id):
        self.name = name.title()
        self.lastname = lastname.title()
        self.dni = dni
        self.email = email
        self.password = password
        self.country_id = country_id  
        self.state_id = state_id  

class Enterprises(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    username = db.Column(db.String(50), unique=True,nullable=False)
    email = db.Column(db.String(150),unique=True,nullable=False)
    dni = db.Column(db.String(50), unique=True,nullable=False)
    description = db.Column(db.String(500),nullable=True)
    phone = db.Column(db.String(150), unique=True, nullable=True)
    level = db.Column(db.Integer, default=1)
    calification = db.Column(db.Float, default=0)
    password = db.Column(db.String(150), nullable=False)
    is_logged = db.Column(db.Boolean, default=False, nullable=False)
    products = db.relationship('Products', backref='product_owner', primaryjoin="Enterprises.id == Products.enterprise_id")
    logo = db.Column(db.String(200), nullable=True)
    country_id = db.Column(db.Integer, db.ForeignKey('countries.id'),nullable=False)
    state_id = db.Column(db.Integer, db.ForeignKey('states.id'),nullable=False)
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow)
    updated_at = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)

    def __init__(self,name,username,dni,email,password,country_id,state_id):
        self.name = name.title()
        self.username = username
        self.dni = dni
        self.email = email
        self.password = password
        self.country_id = country_id
        self.state_id = state_id



class Products(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100),nullable=False)
    description = db.Column(db.String(800),nullable=False)
    price = db.Column(db.Integer,nullable=False)
    enterprise_id = db.Column(db.Integer, db.ForeignKey('enterprises.id'),nullable=False)
    country_id = db.Column(db.Integer, db.ForeignKey(Countries.id, ondelete='CASCADE'), nullable=True)
    state_id = db.Column(db.Integer, db.ForeignKey(States.id, ondelete='CASCADE'), nullable=True)
    images = db.relationship('Products_images', backref='product_img',cascade='all, delete-orphan', lazy='dynamic')
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow)
    updated_at = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)

    def __init__(self,title,description,price,enterprise_id):
        self.title = title.title()
        self.description = description.title()
        self.price = price
        self.enterprise_id = enterprise_id


class Products_images(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    filename = db.Column(db.String(80),nullable=False)
    product_id = db.Column(db.Integer, db.ForeignKey('products.id'),nullable=False)
    def __init__(self,filename,product_id):
        self.filename = filename
        self.product_id = product_id

class Comments(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    comment = db.Column(db.String(255),nullable=False)
    product_id = db.Column(db.Integer, db.ForeignKey('products.id'),nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'),nullable=False)
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow)
    updated_at = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)

