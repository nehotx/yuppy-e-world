from flask import (
    Blueprint,
    render_template,
    request,
    session,
    redirect,
    url_for,
    flash,
)
from werkzeug.utils import secure_filename

from models.models import *

from utils.db import db

from utils.pictures import (
    random_name,
    remove_product_picture,
    remove_user_picture,
)
from utils.services import *

users = Blueprint('users',__name__, url_prefix="/users")

@users.before_request
def before_request():
    if not "user" in session:
        flash("Debes iniciar sessión", "auth_error")
        return redirect(url_for("auth.enterprise_login"))
    elif "user" in session:
        global page_cant
        page_cant = 24
        global sessionUser
        sessionUser = Users.query.filter_by(
            email=session["user"]
        ).first()
        if sessionUser == None:
            session.pop("user", None)
            flash("Ha ocurrido un error en los servidores 😕", "forbiden")
            return redirect(url_for("user_login"))
        elif sessionUser.level < 1:
            flash("Probablemente tu cuenta esta inhabilitada o betada 😕", "forbiden")
            return redirect(url_for("user_login"))

@users.route('/homepage/<int:page_num>')
def homepage(page_num):
    products = Products.query.paginate(
        per_page=page_cant, page=page_num, error_out=True
    )
    images = Products_images.query.all()
    enterprises = Enterprises.query.all()
    return render_template('/users/panel/homepage.html', sessionUser = sessionUser,enterprises=enterprises, products=products, images=images)

@users.route("/my-profile/update/description", methods=["POST"])
def update_description():
    back = request.referrer
    sessionUser.description = request.form["description"]
    db.session.commit()
    flash("Descripcion actualizada con exito", "success")
    return redirect(back)


# Funcion para detectar Extencion de archivo valido

ALLOWED_EXTENSIONS = set(["png", "jpg", "jpeg", "gif", "webp"])


def allowed_extensions(filename):
    return "." in filename and filename.rsplit(".", 1)[1] in ALLOWED_EXTENSIONS


@users.route("/my-profile/logo", methods=["POST"])
def update_logo():
    back = request.referrer
    f = request.files["logo"]

    ext = f.filename.split(".")[1]
    dni = sessionUser.dni

    if f and allowed_extensions(f.filename):
        filename = secure_filename(random_name(dni) + "." + ext)
        f.save("uploads/profile_pictures/users/" + filename)
        update_user_picture(dni, filename)
        flash("Perfil actualizado exitosamente", "success")

        return redirect(back)
