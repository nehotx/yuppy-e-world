from flask import (
    Blueprint,
    render_template,
    request,
    session,
    redirect,
    url_for,
    flash,
)
from werkzeug.utils import secure_filename

from models.models import *

from utils.db import db

from utils.pictures import (
    random_name,
    remove_product_picture,
    remove_enterprise_picture,
)
from utils.services import *

enterprises = Blueprint("enterprises", __name__, url_prefix="/enterprises")


@enterprises.before_request
def before_request():
    if not "enterprise" in session:
        flash("Debes iniciar sessión", "auth_error")
        return redirect(url_for("auth.enterprise_login"))
    elif "enterprise" in session:
        global sessionUser
        global page_cant
        page_cant = 24
        sessionUser = Enterprises.query.filter_by(
            email=session["enterprise"]
        ).first()
        if sessionUser == None:
            session.pop("enterprise", None)
            flash("Ha ocurrido un error en los servidores 😕", "forbiden")
            return redirect(url_for("auth.enterprise_login"))
        elif sessionUser.level < 1:
            flash("Probablemente tu cuenta esta inhabilitada o betada 😕", "forbiden")
            return redirect(url_for("auth.enterprise_login"))


@enterprises.route("/homepage/<int:page_num>")
def homepage(page_num):
    products = Products.query.filter_by(enterprise_id=sessionUser.id).paginate(
        per_page=page_cant, page=page_num, error_out=True
    )
    for product in products:
        if product:
            print(products)

    images = Products_images.query.all()
    return render_template(
        "enterprises/panel/homepage.html",
        sessionUser=sessionUser,
        images=images,
        products=products
    )

@enterprises.route("/my-profile/update/description", methods=["POST"])
def update_description():
    back = request.referrer
    sessionUser.description = request.form["description"]
    db.session.commit()
    flash("Descripcion actualizada con exito", "success")
    return redirect(back)


# Funcion para detectar Extencion de archivo valido

ALLOWED_EXTENSIONS = set(["png", "jpg", "jpeg", "gif", "webp"])


def allowed_extensions(filename):
    return "." in filename and filename.rsplit(".", 1)[1] in ALLOWED_EXTENSIONS


@enterprises.route("/my-profile/logo", methods=["POST"])
def update_logo():
    back = request.referrer
    f = request.files["logo"]

    ext = f.filename.split(".")[1]
    username = sessionUser.username

    if f and allowed_extensions(f.filename):
        filename = secure_filename(random_name(username) + "." + ext)
        f.save("uploads/profile_pictures/enterprises/" + filename)
        update_enterprise_picture(username, filename)
        flash("Perfil actualizado exitosamente", "success")

        return redirect(back)

@enterprises.route("/product/create", methods=["GET", "POST"])
def product_create():
    if request.method == "POST":
        title = request.form["title"]
        description = request.form["description"]
        price = request.form["price"]
        enterprise_id = sessionUser.id
        new_product = Products(title, description, price, enterprise_id)
        db.session.add(new_product)
        db.session.commit()
        product = (
            Products.query.filter_by(enterprise_id=enterprise_id)
            .filter_by(title=title)
            .filter_by(description=description)
            .first()
        )

        # post photos
        session["actual_product"] = product.id

        files = request.files.getlist("files[]")
        i = 0
        for f in files:
            if f and allowed_extensions(f.filename):
                ext = f.filename.split(".")[1]
                filename = secure_filename(
                    random_name(str(session["actual_product"])) + str(i) + "." + ext
                )
                f.save("uploads/products_pictures/" + filename)
                i += 1
                new_photo = Products_images(filename, str(session["actual_product"]))
                db.session.add(new_photo)
                db.session.commit()

        session.pop("actual_product", None)
        flash("Producto Creado exitosamente", "success")
        return redirect(url_for("enterprises.homepage", page_num=1))
    return render_template(
        "/enterprises/panel/products/create.html", sessionUser=sessionUser
    )


@enterprises.route("/product/delete/<id>", methods=["GET", "POST"])
def product_delete(id):
    product = Products.query.get(id)
    images = Products_images.query.filter_by(product_id=product.id).all()
    if request.method == "POST":
        for image in images:
            remove_product_picture(image.filename)
            db.session.delete(image)
            db.session.commit()
        db.session.delete(product)
        db.session.commit()
        flash("Producto eliminado satisfactoriamente", "success")
        return redirect(url_for("enterprises.homepage", page_num=1))

    return render_template(
        "/enterprises/panel/products/delete.html",
        sessionUser=sessionUser,
        product=product,
        images=images
    )


@enterprises.route("/product/show/<id>")
def product_show(id):
    product = Products.query.get(id)
    images = Products_images.query.filter_by(product_id=product.id).all()

    return render_template(
        "/enterprises/panel/products/show.html",
        sessionUser=sessionUser,
        product=product,
        images=images
    )


@enterprises.route("/product/update/<id>", methods=["GET", "POST"])
def product_update(id):
    product = Products.query.get(id)
    if request.method == "POST":
        product.title = request.form["title"]
        product.description = request.form["description"]
        product.price = request.form["price"]
        product.enterprise_id = sessionUser.id
        db.session.commit()

        files = request.files.getlist("files[]")
        fi = Products_images.query.filter_by(product_id=product.id).first()

        if request.files.getlist("files[]") != None:
            i = 0
            for f in files:
                if f and allowed_extensions(f.filename):
                    ext = f.filename.split(".")[1]
                    filename = secure_filename(
                        random_name(str(product.id)) + str(i) + "." + ext
                    )
                    remove_product_picture(fi.filename)
                    fi.filename = filename
                    i += 1
                    f.save("uploads/products_pictures/" + filename)

                    db.session.commit()

        flash("Producto Actualizado exitosamente", "success")
        return redirect(url_for("enterprises.homepage", page_num=1))
    return render_template(
        "/enterprises/panel/products/update.html",
        sessionUser=sessionUser,
        product=product
    )