from flask import Blueprint, render_template, request, session, redirect, url_for, flash
from passlib.hash import sha256_crypt
from utils.db import db
from models.models import Countries, States, Users, Enterprises

auth = Blueprint("auth", __name__, url_prefix="/auth")


# Users authentication


@auth.route("/users/signup", methods=["GET", "POST"])
def signup():
    if request.method == "POST":
        name = request.form["name"]
        lastname = request.form["lastname"]
        dni = request.form["dni"]
        email = request.form["email"]
        password = request.form["password"]
        country_id = request.form["country_id"]
        state_id = request.form["state_id"]

        hashed_pw = sha256_crypt.hash(request.form["password"])
        new_user = Users(name, lastname, dni, email, hashed_pw, country_id, state_id)

        db.session.add(new_user)
        db.session.commit()

        return redirect(url_for("auth.login"))

    countries = Countries.query.all()
    states = States.query.all()
    return render_template(
        "/users/auth/signup.html", countries=countries, states=states
    )


@auth.route("/users/login", methods=["GET", "POST"])
def user_login():
    if request.method == "POST":
        user = Users.query.filter_by(email=request.form["email"]).first()
        if user and sha256_crypt.verify(request.form["password"], user.password):
            session["user"] = user.email
            if user.level == 10:
                return redirect(url_for("admin.countries", page_num=1))
            else:
                return redirect(url_for("users.homepage", page_num=1))
        else:
            flash(
                "Credenciales invalidas por favor verifique e intentelo de nuevo",
                "error",
            )
            return redirect(url_for("auth.user_login"))

    return render_template("/users/auth/login.html")


@auth.route("/user/logout")
def logout():
    session.pop("user", None)

    return redirect(url_for("auth.user_login"))


# Enterprises authentication


@auth.route("/enterprises/signup", methods=["GET", "POST"])
def enterprise_signup():
    enterprises = Enterprises.query.all()
    if request.method == "POST":
        name = request.form["name"]
        error = [False, False, False]

        for enterprise in enterprises:

            if enterprise.username == "@" + request.form["username"]:
                error[0] = True

            elif enterprise.email == request.form["email"]:
                error[1] = True

            elif enterprise.dni == request.form["dni"]:
                error[2] = True

        if error[0] == False and error[1] == False and error[2] == False:
            username = "@" + request.form["username"]
            email = request.form["email"]
            dni = request.form["dni"]

        elif error[0] == True:
            flash("Nombre de usuario no disponible", "error")
            return redirect(url_for("auth.enterprise_signup"))

        elif error[1] == True:
            flash("Correo electronico no disponible", "error")
            return redirect(url_for("auth.enterprise_signup"))

        elif error[2] == True:
            flash("Dni no disponible", "error")
            return redirect(url_for("auth.enterprise_signup"))

        password = request.form["password"]
        country_id = request.form["country_id"]
        state_id = request.form["state_id"]

        hashed_pw = sha256_crypt.hash(request.form["password"])
        new_enterprise = Enterprises(
            name, username, dni, email, hashed_pw, country_id, state_id
        )

        db.session.add(new_enterprise)
        db.session.commit()

        return redirect(url_for("auth.enterprise_login"))

    countries = Countries.query.all()
    states = States.query.all()
    return render_template(
        "/enterprises/auth/signup.html", countries=countries, states=states
    )


@auth.route("/enterprises/login", methods=["GET", "POST"])
def enterprise_login():
    if request.method == "POST":
        enterprise = Enterprises.query.filter_by(email=request.form["email"]).first()
        if enterprise and sha256_crypt.verify(
            request.form["password"], enterprise.password
        ):
            session["enterprise"] = enterprise.email
            return redirect(url_for("enterprises.homepage" , page_num=1))
        else:
            flash(
                "Credenciales invalidas por favor verifique e intentelo de nuevo",
                "error",
            )
            return redirect(url_for("auth.enterprise_login"))
    if "enterprise" in session:
        return redirect(url_for("enterprises.homepage", page_num=1))
    else:
        return render_template("/enterprises/auth/login.html")


@auth.route("/enterprise/logout")
def enterprise_logout():
    session.pop("enterprise", None)

    return redirect(url_for("auth.enterprise_login"))
