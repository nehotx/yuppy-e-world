from flask import Blueprint, render_template,request,redirect,url_for,flash,session
from utils.db import db
from passlib.hash import sha256_crypt
from models.models import Countries,States,Users,Enterprises


admin = Blueprint('admin',__name__)

@admin.before_request
def before_request():
    global sessionUser 
    sessionUser = Users.query.filter_by(email = session['user']).first()
    if not 'user' in session:
        flash('Necesitas iniciar session','error') 
        return redirect(url_for('auth.user_login'))
    else:
        if sessionUser.level < 10:
            flash('No cumples con los requisitos para acceder','forbiden') 
            return redirect(url_for('users.homepage'))



#Countries Functions
@admin.route('/admin/countries/<int:page_num>')
def countries(page_num):
    countries = Countries.query.order_by(Countries.id).paginate(per_page=20, page=page_num, error_out=True)
    return render_template('admin/countries/countries.html', countries=countries, sessionUser = sessionUser)

@admin.route('/admin/country/show/<id>')
def country_show(id):
    country = Countries.query.get(id)
    return render_template('admin/countries/show.html', country=country, sessionUser = sessionUser)

@admin.route('/admin/country/create',methods=['GET','POST'])
def country_create():
    if request.method == 'POST':
        name = request.form['name']
        new_country = Countries(name)
        db.session.add(new_country)
        db.session.commit()
        flash('Pais Creado exitosamente','success')
        return redirect(url_for('admin.countries',page_num=1))
        
    return render_template('admin/countries/create.html', sessionUser = sessionUser)

@admin.route('/admin/country/update/<id>',methods =['POST','GET'])
def country_update(id):
    country = Countries.query.get(id)
    if request.method == 'POST':
        country.name = request.form['name']
        db.session.commit()

        flash('Pais actualizado exitosamente', 'success')

        return redirect(url_for('admin.countries',page_num=1))
    
    return render_template('admin/countries/update.html', country = country, sessionUser = sessionUser)

@admin.route('/admin/country/delete/<id>', methods =['POST','GET'])
def country_delete(id):
    country = Countries.query.get(id)
    if request.method == 'POST':
        db.session.delete(country)
        db.session.commit()

        flash('Pais borrado exitosamente','success')

        return redirect(url_for('admin.countries',page_num=1))
    
    return render_template('admin/countries/delete.html', country = country, sessionUser = sessionUser)


#States Functions
@admin.route('/admin/states/<int:page_num>')
def states(page_num):
    states = States.query.order_by(States.id).paginate(per_page=20, page=page_num, error_out=True)
    countries = Countries.query.all()
    
    return render_template('admin/states/states.html', states=states,countries=countries, sessionUser = sessionUser)

@admin.route('/admin/state/show/<id>')
def state_show(id):
    state = States.query.get(id)
    country = Countries.query.filter(Countries.id == state.country_id).first()
    
    return render_template('admin/states/show.html', state=state, country=country, sessionUser = sessionUser)

@admin.route('/admin/create/state',methods=['GET','POST'])
def state_create():
    if request.method == 'POST':
        name = request.form['name']
        country_id = request.form['country_id']
        new_state = States(name,country_id)
        db.session.add(new_state)
        db.session.commit()
        return redirect(url_for('admin.states',page_num=1))
    countries = Countries.query.all()
    
    return render_template('admin/states/create.html', countries=countries, sessionUser = sessionUser)

@admin.route('/admin/state/update/<id>',methods =['POST','GET'])
def state_update(id):
    state = States.query.get(id)
    countries = Countries.query.all()
    if request.method == 'POST':
        state.name = request.form['name']
        state.country_id = request.form['country_id']
        db.session.commit()

        flash('Estado actualizado exitosamente', 'success')

        return redirect(url_for('admin.states',page_num=1))

    
    return render_template('admin/states/update.html', state = state, countries=countries, sessionUser = sessionUser)

@admin.route('/admin/state/delete/<id>', methods =['POST','GET'])
def state_delete(id):
    state = States.query.get(id)
    if request.method == 'POST':
        db.session.delete(state)
        db.session.commit()

        flash('Estado borrado exitosamente','success')

        return redirect(url_for('admin.states',page_num=1))
    
    return render_template('admin/states/delete.html', state = state, sessionUser = sessionUser)


#User Functions

@admin.route('/admin/users/<int:page_num>')
def users(page_num):
    users = Users.query.order_by(Users.id).paginate(per_page=20, page=page_num, error_out=True)
    countries = Countries.query.all()
    return render_template('admin/users/users.html', users=users, countries=countries, sessionUser=sessionUser)

@admin.route('/admin/user/show/<id>')
def user_show(id):
     user = Users.query.filter_by(id = id).first()
     country = Countries.query.filter(Countries.id == user.country_id).first()
     state = States.query.filter(States.id == user.state_id).first()
     return render_template('admin/users/show.html', country=country,state=state, user=user, sessionUser=sessionUser)

@admin.route('/admin/user/update/<id>',methods =['POST','GET'])
def user_update(id):
    user = Users.query.get(id)
    state = States.query.filter_by(id = user.state_id).first()
    country = Countries.query.filter_by(id = user.country_id).first()
    states = States.query.all()
    countries = Countries.query.all()
    print(state)
    if request.method == 'POST':
        user.dni = request.form['dni']
        user.email = request.form['email']
        if request.form['phone'] != '':
            user.phone = request.form['phone']
            
        user.level = request.form['level']
        user.calification = request.form['calification']
        user.country_id = request.form['country_id']
        user.state_id = request.form['state_id']

        if user.password != None:
            user.password = sha256_crypt.hash(request.form['password'])
        
            
        db.session.commit()

        flash('Usuario actualizado exitosamente', 'success')

        return redirect(url_for('admin.users',page_num=1))

    return render_template('admin/users/update.html', state=state ,states=states, country=country, countries=countries, user=user, sessionUser=sessionUser)

@admin.route('/admin/user/delete/<id>', methods =['POST','GET'])
def user_delete(id):
    user = Users.query.get(id)
    if request.method == 'POST':
        db.session.delete(user)
        db.session.commit()

        flash('User borrado exitosamente','success')

        return redirect(url_for('admin.users',page_num=1))
    return render_template('admin/users/delete.html', user = user, sessionUser = sessionUser)


#Enterprises Functions

@admin.route('/admin/enterprises/<int:page_num>')
def enterprises(page_num):
    enterprises = Enterprises.query.order_by(Enterprises.id).order_by(Enterprises.country_id).paginate(per_page=20, page=page_num, error_out=True)
    countries = Countries.query.all()
    return render_template('admin/enterprises/enterprises.html', enterprises=enterprises, countries=countries, sessionUser=sessionUser)

@admin.route('/admin/enterprise/show/<id>')
def enterprise_show(id):
     enterprise = Enterprises.query.filter_by(id = id).first()
     country = Countries.query.filter(Countries.id == enterprise.country_id).first()
     state = States.query.filter(States.id == enterprise.state_id).first()
     return render_template('admin/enterprises/show.html', country=country,state=state, enterprise=enterprise, sessionUser=sessionUser)

@admin.route('/admin/enterprise/update/<id>',methods =['POST','GET'])
def enterprise_update(id):
    enterprise = Enterprises.query.get(id)
    state = States.query.filter_by(id = enterprise.state_id).first()
    country = Countries.query.filter_by(id = enterprise.country_id).first()
    states = States.query.all()
    countries = Countries.query.all()
    print(state)
    if request.method == 'POST':
        enterprise.name = request.form['name']
        enterprise.username = request.form['username']
        enterprise.dni = request.form['dni']
        enterprise.email = request.form['email']
        if request.form['phone'] != '':
            enterprise.phone = request.form['phone']
            
        enterprise.level = request.form['level']
        enterprise.calification = request.form['calification']
        enterprise.country_id = request.form['country_id']
        enterprise.state_id = request.form['state_id']

        if enterprise.password != None:
            enterprise.password = sha256_crypt.hash(request.form['password'])
        
            
        db.session.commit()

        flash('Negocio actualizado exitosamente', 'success')

        return redirect(url_for('admin.enterprises',page_num=1))

    return render_template('admin/enterprises/update.html', state=state, country=country, countries=countries, enterprise=enterprise, sessionUser=sessionUser)

@admin.route('/admin/enterprise/delete/<id>', methods =['POST','GET'])
def enterprise_delete(id):
    enterprise = Enterprises.query.get(id)
    if request.method == 'POST':
        db.session.delete(enterprise)
        db.session.commit()

        flash('Negocio borrado exitosamente','success')

        return redirect(url_for('admin.enterprises',page_num=1))
    return render_template('admin/enterprises/delete.html', enterprise = enterprise, sessionUser = sessionUser)