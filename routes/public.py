from flask import Blueprint, jsonify
from utils.db import db
from models.models import Countries,States,Users,Enterprises

public = Blueprint('public',__name__)

@public.route('/states/<id>')
def countryStates(id):
    states = States.query.filter_by(country_id = id).all()

    stateArray = []
    for state in states:
        stateObj = {}
        stateObj['id'] = state.id
        stateObj['name'] = state.name 
        stateObj['country_id']= state.country_id
        stateArray.append(stateObj)
    
    return jsonify({'states': stateArray})

@public.route('/enterprises/<username>')
def enterpriseJsonify(username):
    enterprise = Enterprises.query.filter_by(username = username).first()

    enterpriseArray = []

    enterpriseObj ={
        'id' : enterprise.id,
        'dni' : enterprise.dni,
        'email': enterprise.email,
        'username' : enterprise.username
    }
    enterpriseArray.append(enterpriseObj)

    return jsonify({'enterprise': enterpriseArray})