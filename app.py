from flask import (
    Flask,
    request,
    render_template,
    url_for,
    redirect,
    send_from_directory,
    session,
    flash,
)
from flask_sqlalchemy import SQLAlchemy
from models.models import *

# importing routes
from routes.admin import admin
from routes.users import users
from routes.enterprises import enterprises
from routes.public import public
from routes.auth import auth

app = Flask(__name__)
app.config[
    "SECRET_KEY"
] = "eyJhbGciOiJIUzI1NiJ9.eyJhIjoiRXN0YSBlcyBtaSBjbGF2ZSBzZWNyZXRhIHBhcmEgcXVlIHNlYSBpbXBvc2libGUgbGEgdm95IGEgZW5jcmlwdGFyIn0.Z6Zdrgs7r4onGFslgWtEqecyme3Ni-QgYiiSYeaYQvU"
app.config["SQLALCHEMY_DATABASE_URI"] = "mysql://root@localhost:3306/yupy-ecomerce_db"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False


@app.route("/")
def index():
    enterprises = Enterprises.query.paginate(
        per_page=6,error_out=True
    )
    return render_template("landing_page.html", enterprises=enterprises)


@app.route("/profile/picture/<path:filename>")
def picture_enterprise_profile(filename):
    base_url = "uploads/profile_pictures/enterprises/"
    if filename == None:
        filename == "user_default.png"
    return send_from_directory(base_url, filename)

@app.route("/profile/users/picture/<path:filename>")
def picture_user_profile(filename):
    base_url = "uploads/profile_pictures/users/"
    if filename == "none":
        filename == "user_default.png"
    return send_from_directory(base_url, filename)


@app.route("/product/picture/<path:filename>")
def picture_products(filename):
    base_url = "uploads/products_pictures/"
    return send_from_directory(base_url, filename)

    
def biBefReq():
    global page_cant
    page_cant = 24
    global is_user
    global sessionUser
    if "enterprise" in session:
        is_user = False
        sessionUser = Enterprises.query.filter_by(email=session["enterprise"]).first()
        if sessionUser == None:
            session.pop("enterprise", None)
            flash("Ha ocurrido un error en los servidores 😕", "forbiden")
            return redirect(url_for("auth.enterprise_login"))

    elif "user" in session:
        is_user = True
        sessionUser = Users.query.filter_by(email=session["user"]).first()
        if sessionUser == None:
            session.pop("user", None)
            flash("Ha ocurrido un error en los servidores 😕", "forbiden")
            return redirect(url_for("auth.user_login"))


@app.route("/enterprises/profile/<id>/<int:page_num>")
def profile(id, page_num):
    biBefReq()
    enterprise = Enterprises.query.get(id)
    products = Products.query.filter_by(enterprise_id=id).paginate(
        per_page=page_cant, page=page_num, error_out=True
    )

    images = Products_images.query.all()
    return render_template(
        "enterprises/panel/profile.html",
        sessionUser=sessionUser,
        images=images,
        products=products,
        enterprise=enterprise,
        is_user=is_user
    )

@app.route("/enterprises/product/show/<id>")
def product_show(id):
    biBefReq()
    product = Products.query.get(id)
    enterprise = Enterprises.query.filter_by(id = product.enterprise_id).first()
    images = Products_images.query.filter_by(product_id=product.id).all()

    return render_template(
        "/enterprises/panel/products/show.html",
        sessionUser=sessionUser,
        product=product,
        images=images,
        enterprise=enterprise,
        is_user=is_user
    )

@app.route("/users/profile/<id>")
def user_profile(id):
    biBefReq()
    user = Users.query.get(id)
    return render_template(
        "users/panel/profile.html",
        sessionUser=sessionUser,
        user=user,
        is_user=is_user
    )

@app.route("/search", methods=["GET", "POST"])
def search():
    biBefReq()
    if request.method == "POST":
        searched = request.form["search"]
        products = Products.query.filter(
            Products.title.like("%" + searched + "%")
        ).order_by(Products.title
        ).all()
        enterprises = Enterprises.query.filter(
            Enterprises.name.like("%" + searched + "%")
        ).order_by(Enterprises.name
        ).all()
        images = Products_images.query.all()
        creators = Enterprises.query.all()
        return render_template(
            "search.html",
            searched=searched,
            products=products,
            images=images,
            creators=creators,
            enterprises=enterprises,
            sessionUser=sessionUser,
            is_user=is_user
        )


SQLAlchemy(app)

app.register_blueprint(admin)
app.register_blueprint(users)
app.register_blueprint(enterprises)
app.register_blueprint(public)
app.register_blueprint(auth)
