/* States verify Changes */
const state_select   = document.getElementById('state_select');
const country_select = document.getElementById('country_select');

country_select.onchange = function(){

    const country = country_select.value;

    fetch('/states/' + country).then(function(response){

        response.json().then(function(data){
            let optionHTML ='';
            optionHTML += '<option selected disabled value="">-- </option>';
            for(let state of data.states){
                optionHTML += '<option value="' + state.id + '">' + state.name + '</option>';
            }
            state_select.innerHTML = optionHTML;
        });
    });

}