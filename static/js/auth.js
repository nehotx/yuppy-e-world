const toggleButton = document.getElementById('toggleButton');
const inputPassword = document.getElementById('inputPassword');
const inputPassword1 = document.getElementById('inputPassword1');
const formAction = document.getElementById('formAction');
//Error div
const errorElement = document.getElementById('error');
const text_password = document.getElementById('text_password');

//Inputs
// const usernameInput = document.getElementById('usernameInput');
// const emailInput = document.getElementById('emailInput');
// const dniInput = document.getElementById('dniInput');

// const text_username = document.getElementById('text_username');
// const text_dni = document.getElementById('text_dni');
// const text_email = document.getElementById('text_email');

toggleButton.addEventListener('click', () => {

    if (inputPassword.type == 'password') {
        toggleButton.classList.remove('fa-eye');
        toggleButton.classList.add('fa-eye-slash');
        inputPassword.type = 'text';
        inputPassword1.type = 'text';
    } else if (inputPassword.type == 'text') {
        toggleButton.classList.remove('fa-eye-slash');
        toggleButton.classList.add('fa-eye');
        inputPassword.type = 'password';
        inputPassword1.type = 'password';
    }
});

//Anotacion Nueva Probar funcion en auth.py y si no funciona intentar con la opcion de abajo
// ANOTACION BUSCAR FLASK-MARSHMALLOW PORQUE NECESITO BUSCAR ENTRE TODOS LOS USUARIOS LOS CAMPOS RELLENADOS

formAction.addEventListener('submit', (e) => {
    const timer = 8000;

    if (inputPassword.value != inputPassword1.value) {
        errorElement.classList.remove('d-none');
        text_password.classList.remove('d-none');
        setTimeout(() => {
            errorElement.classList.add('d-none');
            text_password.classList.add('d-none');
        }, timer)
        e.preventDefault();
    }

});
